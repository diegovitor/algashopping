import { configureStore } from "@reduxjs/toolkit";
import CalculatorReducer from "./Calculator/Calculator.reducer";
import { combineReducers } from "@reduxjs/toolkit";
import ProductsReducer from "./Products/Products.reducer";

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

const rootReducer = combineReducers({
    calculator : CalculatorReducer,
    products: ProductsReducer
  });

const persistedReducer = persistReducer(
  {
    key: 'algashopping',
    storage
  }, rootReducer
)
export const store = configureStore({
    reducer : persistedReducer,
})

export const perStore = persistStore(store);